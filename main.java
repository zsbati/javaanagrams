public class Main {

    static boolean isAnagram(String a, String b) {
        // Complete the function
        a = a.toLowerCase();
        b = b.toLowerCase();
        if (a.length() != b.length()){
          return false;
        }
        
        char letter = 'a';
        int countA = 0;
        int countB = 0;
        for (int i=0; i<a.length(); i++){
          letter = a.charAt(i);
          countA = 0;
          countB = 0;
          for (int j=0; j<a.length(); j++){
            if (a.charAt(j)==letter){
              countA += 1;
            }
            if (b.charAt(j)==letter){
              countB += 1;
            }
          }
          if (countA != countB){
            return false;
          }
        }
        
        return true;
    }

    public static void main(String[] args) {
    
        
        String a = "putty";
        String b = "yttUp";
        
        boolean ret = isAnagram(a, b);
        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
    }
}
